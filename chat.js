var socket = io.connect('http://localhost:8080');

var messagesBox = document.querySelector('.messages-output .messages-box');
var messageInput = document.querySelector('.messages-control .message-input');
var messageSubmit = document.querySelector('.messages-control .message-submit');

var addMessage = function(message) {
  var tag = document.createElement('p');
  var text = document.createTextNode(message);

  tag.appendChild(text);

  messagesBox.append(tag);
}

var scrollToEnd = function() {
  messagesBox.scrollTop = messagesBox.scrollHeight;
};

var sendMessageFromInput = function() {
  var message = messageInput.value;

  addMessage('Eu: ' + message);
  socket.emit('message:client', {whom: 'user', message: message});

  messageInput.value = '';
  scrollToEnd();
};

messageSubmit.addEventListener('click', sendMessageFromInput);
messageInput.addEventListener('keydown', function(e) {
  if (e.keyCode == 13)
    sendMessageFromInput();
});

socket.on('message:server', function(data) {
  message = data.whom + ": " + data.message;
  addMessage(message);
  scrollToEnd();
});
